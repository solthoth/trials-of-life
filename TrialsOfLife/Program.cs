﻿using System;

namespace TrialsOfLife
{
    class Program
    {
        static void Main(string[] args)
        {
            Human male = new Human("Carlos");
            Human female = new Human(null);
            Person boy = new Person("Aidan");
            Person girl = new Person("Regina");

            boy.Name = "Angel";
            female.setName("Nora");

            IntroducePerson(male);
            IntroducePerson(female);
            IntroducePerson(boy);
            IntroducePerson(girl);

            Dog myPuppy = new Dog();

            SaySomething(male);
            SaySomething(female);
            SaySomething(boy);
            SaySomething(girl);
            SaySomething(myPuppy);

            CreatureFactory factory = new CreatureFactory();
            for (int i = 0; i < 10; i++)
            {
                ITalking talkingCreature = factory.Create();
                SaySomething(talkingCreature);
            }            
        }

        public static void SaySomething(ITalking talkable)
        {
            talkable.Speak();
        }

        public static void DoesThisEntityExist(Being being)
        {
            Console.WriteLine($"Do you exist? {being.Exists()}");
        }

        public static void IntroducePerson(Human person)
        {
            person.Greeting();
            Console.WriteLine("This human's name is " + person.getName());

            if (person.GetType() == typeof(Person))
            {
                Person existingPerson = (Person)person;
                existingPerson.Birthday();
                existingPerson.Birthday();
                existingPerson.Birthday();
                existingPerson.Birthday();
                int age = existingPerson.Birthday();
                Console.WriteLine("This peron's age is " + age.ToString());
            }
            else
            {
                Console.WriteLine("This human is not a person");
                try
                {
                    person.Die();
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Something has happened; {ex.Message}");
                }
            }
            DoesThisEntityExist(person);
        }
    }
}
