﻿using System;

namespace TrialsOfLife
{
    public class Dog : ITalking
    {
        public void Speak()
        {
            Console.WriteLine("Bark, bark!");
        }
    }
}
