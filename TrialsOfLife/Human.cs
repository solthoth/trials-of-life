﻿using System;

namespace TrialsOfLife
{
    public class Human : Being, ITalking
    {
        private string _name;

        public Human(string name)
        {
            _name = name;
            Age = 0;
        }

        public virtual void Greeting()
        {
            Console.WriteLine("Hello, my name is " + this._name);
        }

        public string getName()
        {
            return _name;
        }

        public void setName(string value)
        {
            _name = value;
        }

        protected int Age;

        public override void Die()
        {
            throw new Exception("Human has died");
        }

        public void Speak()
        {
            Console.WriteLine("Mumble, mumble");
        }
    }
}
