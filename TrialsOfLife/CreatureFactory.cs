﻿using System;

namespace TrialsOfLife
{
    public class CreatureFactory
    {
        public ITalking Create()
        {
            int counter = new Random(10).Next(0, 10);
            if (counter % 2 == 0)
            {
                return new Person();
            }
            else
            {
                return new Dog();
            }
        }
    }
}
