﻿using System;

namespace TrialsOfLife
{
    public class Person : Human
    {
        public Person() : base(null)
        {
            base.Age = 10;
        }

        public Person(string name) : base(name)
        {
            base.Age = 11;
        }

        public int Birthday()
        {
            Age += 1;
            return Age;
        }

        public string Name
        {
            get
            {
                return getName();
            }
            set
            {
                setName(value);
            }
        }

        public override void Greeting()
        {
            Console.WriteLine("Hello, this is my name: " + Name);
        }
    }
}
