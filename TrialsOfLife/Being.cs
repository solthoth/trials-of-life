﻿namespace TrialsOfLife
{
    public abstract class Being
    {
        public Being()
        {

        }

        public bool Exists()
        {
            return true;
        }

        public abstract void Die();
    }
}
